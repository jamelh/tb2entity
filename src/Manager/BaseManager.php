<?php

namespace Drupal\tb2entity\Manager;

use Drupal\Core\Database\Database;

/**
 * Class BaseManager.
 */
class BaseManager  {

    /**
     * {@inheritdoc}
     */
    public function getAllEntity($prefixe = null) {

        return $tables = Database::getConnection()->schema()->findTables($prefixe.'%');

    }

}
