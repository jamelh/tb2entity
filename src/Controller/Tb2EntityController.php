<?php

/**
 *
 */

namespace Drupal\tb2entity\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\tb2entity\Form\FilterForm;


/**
 * Class Tb2EntityController
 * @package Drupal\tb2entity\Controller
 */
class Tb2EntityController extends ControllerBase
{
    public function content()
    {
        return array(
            '#theme' => 'tb2entity',
        );
    }

    /**
     * @return array
     */
    public function customConfig()
    {
        return array(
            '#theme' => 'customConfig',
        );
    }
}