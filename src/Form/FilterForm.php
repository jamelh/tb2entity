<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\tb2entity\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class FilterForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'tb2entity_forms_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {
        $form['prefixe_tables'] = array(
            '#type' => 'textfield',
            '#title' => t('Le préfixe des tables'),
            '#required' => false,
            '#placeholder' => t('Le préfixe des tables a convertire'),
            '#default_value' => !empty(\Drupal::state()->get('prefixe_tables'))?\Drupal::state()->get('prefixe_tables'):'',
        );

        $form['tables_list'] = array(
            '#type' => 'textarea',
            '#title' => t('Les tables à convertir'),
            '#placeholder' => t('Supparer les tables par , exemple (table1,table2,table3) '),
            '#required' => false,
            '#default_value' => !empty(\Drupal::state()->get('tables_list'))?\Drupal::state()->get('tables_list'):'',
        );
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {

        if(!$form_state->getValue('prefixe_tables') && !$form_state->getValue('tables_list')){
            $form_state->setErrorByName('prefixe_tables', $this->t("Au moins un préfixe ou une table est requis"));
            $form_state->setErrorByName('tables_list', $this->t("Au moins un préfixe ou une table est requis"));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

        foreach ($form_state->getValues() as $key => $value) {
            \Drupal::state()->set($key,$value);
        }
        $form_state->setRedirect('tb2entity_list');
    }
}
