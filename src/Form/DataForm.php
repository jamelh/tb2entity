<?php
/**
 * @file
 * Contains \Drupal\amazing_forms\Form\ContributeForm.
 */

namespace Drupal\tb2entity\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;

/**
 * Contribute form.
 */
class DataForm extends FormBase {
    /**
     * {@inheritdoc}
     */
    public function getFormId() {
        return 'tb2entity_data_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state) {

        $prefixe = \Drupal::state()->get('prefixe_tables');
        $tablesList = \Drupal::state()->get('tables_list');
        $tables = [];
        if(!empty($prefixe))
            $tables = \Drupal::service('tb2entity.manager')->getAllEntity($prefixe);
        $options = [];
        foreach($tables as $ligne){
            $options[]['table_name'] = $ligne;
        }
        $header = [
            'table_name' => t('Table'),
        ];

        $form['table'] = array(
            '#type' => 'tableselect',
            '#header' => $header,
            '#options' => $options,
            '#empty' => ('No users founds'),
        );
        $form['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Submit'),
        );
        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function validateForm(array &$form, FormStateInterface $form_state) {


    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state) {

    }
}
