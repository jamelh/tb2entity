(function($) {
    Drupal.behaviors.tb2entity = {
        attach: function(context) {
            isOneValue("#edit-prefixe-tables","#edit-tables-list");
            isOneValue("#edit-tables-list","#edit-prefixe-tables");
            $("#edit-prefixe-tables").on("input propertychange",function(){
                isOneValue("#edit-prefixe-tables","#edit-tables-list");
            });
            $("#edit-tables-list").on("input propertychange",function(){
                isOneValue("#edit-tables-list","#edit-prefixe-tables");
            });
        }
    }

    /**
     * Disabled the inputIdDisable if the inputIdValue not empty
     * @param inputIdValue
     * @param inputIdDisable
     */
    function isOneValue(inputIdValue,inputIdDisable){
        if($(inputIdValue).val()) {
            $(inputIdDisable).attr('disabled', 'disabled');
        }
        else{
            $(inputIdDisable).removeAttr('disabled');
        }
    }
})(jQuery);
